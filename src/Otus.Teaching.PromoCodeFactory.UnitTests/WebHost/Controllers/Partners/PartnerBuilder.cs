﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private Guid _id;
        private bool _isActive;
        private ICollection<PartnerPromoCodeLimit> _partnerLimits;
        public int _numberIssuedPromoCodes;

        public PartnerBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public PartnerBuilder WithIsActive(bool isActive)
        {
            _isActive = isActive;
            return this;
        }

        public PartnerBuilder WithPartnerLimits(ICollection<PartnerPromoCodeLimit> partnerLimits)
        {
            _partnerLimits = partnerLimits;

            return this;
        }

        public PartnerBuilder WithNumberIssuedPromoCodes(int numberIssuedPromoCodes)
        {
            _numberIssuedPromoCodes = numberIssuedPromoCodes;
            return this;
        }

        public Partner Build()
        {
            return new Partner
            {
                Id = _id,
                IsActive = _isActive,
                PartnerLimits = _partnerLimits,
                NumberIssuedPromoCodes = _numberIssuedPromoCodes
            };
        }
    }
}
