﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnBadRequest()
        {
            //Arrange
            var partner =
                new PartnerBuilder()
                .Build();

            var request =
                new SetPartnerPromoCodeLimitRequestBuilder()
                .Build();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should()
                .BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnBadRequest()
        {
            //Arrange
            var partner =
                new PartnerBuilder()
                .WithId(Guid.NewGuid())
                .WithIsActive(false)
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var request =
                new SetPartnerPromoCodeLimitRequestBuilder()
                .Build();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should()
                .BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerWithPartnerLimitWithEmptyCancelDate_NumberIssuedPromoCodesShouldEqualZero()
        {

            //Arrange
            var partnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimitBuilder()
                .WithId(Guid.NewGuid())
                .Build()
            };

            var partner =
                new PartnerBuilder()
                .WithId(Guid.NewGuid())
                .WithIsActive(true)
                .WithPartnerLimits(partnerLimits)
                .WithNumberIssuedPromoCodes(5)
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner))
                .Returns(Task.Run(() => partner));

            var request =
                new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(3)
                .Build();

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            partner.NumberIssuedPromoCodes.Should()
                .Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerWithPartnerLimitWithEmptyCancelDate_ShouldCancelPreviousLimit()
        {
            //Arrange
            var partnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimitBuilder()
                .WithId(Guid.NewGuid())
                .Build()
            };

            var partner =
                new PartnerBuilder()
                .WithId(Guid.NewGuid())
                .WithIsActive(true)
                .WithPartnerLimits(partnerLimits)
                .WithNumberIssuedPromoCodes(5)
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner))
                .Returns(Task.Run(() => partner));

            var request =
                new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(3)
                .Build();

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            partner.PartnerLimits.First().CancelDate
                .Should()
                .NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerWithPartnerLimitWithEmptyCancelDateAndLimitIsZero_ReturnBadRequest()
        {

            //Arrange
            var partnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimitBuilder()
                .WithId(Guid.NewGuid())
                .Build()
            };

            var partner =
                new PartnerBuilder()
                .WithId(Guid.NewGuid())
                .WithIsActive(true)
                .WithPartnerLimits(partnerLimits)
                .WithNumberIssuedPromoCodes(5)
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner))
                .Returns(Task.Run(() => partner));

            var request =
                new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(0)
                .Build();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should()
                .BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerWithPartnerLimitWithEmptyCancelDateAndLimitIsZero_ShouldAddNewLimit()
        {
            //Arrange
            var partnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimitBuilder()
                .WithId(Guid.NewGuid())
                .Build()
            };

            var partner =
                new PartnerBuilder()
                .WithId(Guid.NewGuid())
                .WithIsActive(true)
                .WithPartnerLimits(partnerLimits)
                .WithNumberIssuedPromoCodes(5)
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner))
                .Returns(Task.Run(() => partner));

            var request =
                new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(5)
                .Build();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should()
                .BeAssignableTo<CreatedAtActionResult>();

            var partnerResult = ((CreatedAtActionResult)result).Value as Partner;

            partnerResult.Should()
                .NotBeNull();

            partnerResult.PartnerLimits.Count
                .Should()
                .Be(2);
        }
    }
}