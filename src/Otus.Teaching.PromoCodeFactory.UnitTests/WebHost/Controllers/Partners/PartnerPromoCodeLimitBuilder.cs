﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerPromoCodeLimitBuilder
    {
        public Guid _id;
        public DateTime? _cancelDate;

        public PartnerPromoCodeLimitBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public PartnerPromoCodeLimitBuilder WithCancelDate(DateTime? cancelDate)
        {
            _cancelDate = cancelDate;
            return this;
        }

        public PartnerPromoCodeLimit Build()
        {
            return new PartnerPromoCodeLimit
            {
                Id = _id,
                CancelDate = _cancelDate
            };
        }
    }
}
